#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>

#include"rendering.h"
#include"datastructs.h"
#include"utils.h"
#include"linkedlist.h"

#define xfree(ptr) {free(ptr); ptr=NULL;}

#define TEXTBOX_X_PADDING 5
#define TEXTBOX_Y_PADDING 3

extern gamestate* gs;
extern char* item_name[];

void render_inventory_screen(){

    SDL_Event event;

    //aiming for a shadow-like thing over part of the screen
    SDL_SetRenderDrawColor(gs->renderer, 0,0,0,200);

    SDL_Rect tmp = {50,50,800-100, 600-100};
    gs->inventory_rect = &tmp;
    SDL_RenderFillRect(gs->renderer, gs->inventory_rect); //background

    //place items.
    int offset = 0;
    List* l = gs->inventory;
    for (Node* nd = l->head; nd != NULL; nd = nd->next, ++offset){
	item_for_inventory_screen((Item*)nd->data, offset);
    }

    SDL_RenderPresent(gs->renderer);
    
    //loop shall eventually handle inventory menu interaction;
    while (1){
	SDL_WaitEvent(&event);
	if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_i){
	    gs->show_inventory = 0;
	    break;
	}
    }
   
}

void item_for_inventory_screen(Item* i, int offset){

    SDL_Colour c = {255,255,255,0};
    SDL_Surface* surface;

    SDL_Rect tmp_rect = {.x = gs->inventory_rect->x+10,
			 .y = gs->inventory_rect->y+30*offset};

    char* item_string = xcalloc(50,sizeof(char));

    snprintf(item_string, 50, "%s:    %i", item_name[i->type], i->quantity );
    surface = TTF_RenderText_Solid(gs->font, item_string, c);

    tmp_rect.h = surface->h;
    tmp_rect.w = surface->w;
    
    SDL_Texture* t = SDL_CreateTextureFromSurface(gs->renderer, surface);
    
    SDL_RenderCopy(gs->renderer, t, NULL, &tmp_rect);
    SDL_FreeSurface(surface);
    xfree(item_string);
}


/*Loads sprites into an Entity's texture bank, finding images by entity name. Do not edit!*/
void load_sprites(char* name, Entity* entity){
    switch (entity->type){
	case PLAYER:
	case NPC:
	    entity->textures = xcalloc(16,sizeof(SDL_Texture*));
	    for (int i=0; i<16; ++i){
		SDL_Surface* temp_surface;
		char* sprite_file_name = xcalloc(50,sizeof(char));

		snprintf(sprite_file_name, 50, "images/%s-%d.png", name, i);
		puts(sprite_file_name);
		temp_surface = IMG_Load(sprite_file_name);
		entity->textures[i] = SDL_CreateTextureFromSurface(gs->renderer, temp_surface);

		xfree(sprite_file_name);
		SDL_FreeSurface(temp_surface);
	    }
	    break;

	case ITEM:
	    entity->textures = xmalloc(sizeof(SDL_Texture*));
	    SDL_Surface* temp_surface;
	    char* sprite_file_name = xcalloc(50,sizeof(char));

	    snprintf(sprite_file_name, 50, "images/%s.png", name);
	    puts(sprite_file_name);
	    temp_surface = IMG_Load(sprite_file_name);
	    entity->textures[0] = SDL_CreateTextureFromSurface(gs->renderer, temp_surface);

	    xfree(sprite_file_name);
	    SDL_FreeSurface(temp_surface);
	    break;

	default:
	    break;
    }
}


void render_screen(){
    //Background
    SDL_SetRenderDrawColor(gs->renderer, 240,240,240,255);
    SDL_RenderClear(gs->renderer);
			    
    //Pure white.
    SDL_SetRenderDrawColor(gs->renderer, 255,255,255,255);

    List* entities = gs->entities;
    
    SDL_Rect player_rect = { gs->player->x, gs->player->y, gs->player->w, gs->player->h };
    SDL_RenderCopy(gs->renderer, gs->player->sprite, NULL, &player_rect);

    for (Node* nd = entities->head; nd != NULL; nd = nd->next){
	Entity* e = (Entity*)nd->data;
	if (e->rendered){
	    SDL_Rect e_rect = { e->x, e->y, e->w, e->h};
	    SDL_RenderCopy(gs->renderer, e->sprite, NULL, &e_rect);
	}
    }
	
    SDL_SetRenderDrawColor(gs->renderer, 255, 0, 0,255);

    SDL_Rect border_rect;
    if (gs->show_text){
	SDL_SetRenderDrawColor(gs->renderer, 0,0,153,255);

	border_rect = (SDL_Rect){ gs->textrect->x-TEXTBOX_X_PADDING,
				  gs->textrect->y-TEXTBOX_Y_PADDING,
				  gs->textrect->w+2*TEXTBOX_X_PADDING,
				  gs->textrect->h+2*TEXTBOX_Y_PADDING };

        SDL_RenderFillRect(gs->renderer, &border_rect); //background
	SDL_SetRenderDrawColor(gs->renderer, 255,0,0,255); //set to red bc white bg.
	SDL_RenderDrawRect(gs->renderer, &border_rect); //border
        SDL_RenderCopy(gs->renderer, gs->texttexture, NULL, gs->textrect); //text
    }

    if (gs->show_inventory){
	render_inventory_screen();
    }
    
    SDL_RenderPresent(gs->renderer);
} 
	

void fill_textbox(char* text){

    SDL_Color colour = {255,255,255,0};
    SDL_Surface* surface;
    
    surface = TTF_RenderText_Blended_Wrapped(gs->font, text, colour, 400);

    gs->texttexture = SDL_CreateTextureFromSurface(gs->renderer, surface);
    gs->textrect->w = surface->w;
    gs->textrect->h = surface->h;

    SDL_FreeSurface(surface);
}


/* The hard work is done behind the scenes, in linkedlist.c. Will resolve the conversation script to 
 * load using Entity name and gamestate flags, and then string_split in linkedlist.c takes the script
 * and returns a list of strings which alternate between an entity name and a line, where the named entity 
 * will say the line.
 */
void perform_dialogue(){
    gs->show_text = 1;
    SDL_Event next_page_event;
    
    //get dialogue from file - later change to something for resolving the right dialogue.
    char* dialogue_file = "dialogue/first_conversation";
    List* script = read_dialogue_file(dialogue_file);
    
    //work through dialogue
    Entity* speaker = gs->player;
    List* entities = gs->entities;
    Node* line = script->head;
    Node* nd = entities->head;

    while (line->next){

	//find speaker.
	if (strstr((char*)line->data, gs->player->name)){
	    speaker = gs->player;
	}
	else {
	    for (; nd != NULL; nd = nd->next){
		Entity* e = (Entity*)nd->data;
		if (e->type != NPC) continue; //whoooops, very bad memory things happen without this check.
		if (strstr((char*)line->data, e->name)){
			speaker = e;
		}
	    }
	}

	//get speaker's line and render box in appropriate place.
	line = line->next; 
	
	fill_textbox((char*)line->data);

	if (speaker->direction == RIGHT){
	    gs->textrect->x = speaker->x - gs->textrect->w - TEXTBOX_X_PADDING;
	    gs->textrect->y = speaker->y + TEXTBOX_Y_PADDING;
	}
	else {
	    gs->textrect->x = speaker->x + speaker->w + TEXTBOX_X_PADDING;
	    gs->textrect->y = speaker->y + TEXTBOX_Y_PADDING;
	}

	render_screen();

	//player can't do shit during a conversation other than work through it.
	while (1){
	    SDL_WaitEvent(&next_page_event);
	    if (next_page_event.type == SDL_KEYDOWN && next_page_event.key.keysym.sym == SDLK_x){
		break;
	    }
	}

	//if speaker's line was last line of dialogue file, break out.
	if (line->next){
	    line = line->next;
	}
	else break;
    }
    
    //Take care of memory!
    list_destroy_include_data(script);
    script = NULL;
    gs->show_text = 0;
}

