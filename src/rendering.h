
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>

#include"datastructs.h"
#include"utils.h"
#include"linkedlist.h"

void fill_textbox(char* text);
void load_sprites(char* name, Entity* entity);
void render_inventory_screen();
void perform_dialogue();
void render_screen();
void item_for_inventory_screen(Item* i, int offset);
