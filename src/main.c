#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>

#include"datastructs.h"
#include"utils.h"
#include"linkedlist.h"
#include"rendering.h"
#include"music.h"
#include"process_events.h"

#define xfree(ptr) {free(ptr); ptr=NULL;}

#define TEXTBOX_X_PADDING 5
#define TEXTBOX_Y_PADDING 3

gamestate* gs;

const char* item_name[] = {"Pheonix Feather"};

#if defined (_WIN32)
	const char* font_file="winass/comic.ttf";
#elif defined(__linux__)
	const char* font_file="/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf";
#endif

int main(int argc, char** argv){

    gs = xmalloc(sizeof(gamestate));

    gs->entity_count = 0;
    gs->time = 0;
    gs->step = 0;
    gs->show_text = 0;
    gs->inventory = new_list();
    gs->show_inventory = 0;
    
    SDL_Rect textrect = {0,0,100,100};
    gs->textrect = &textrect;
    
    if (TTF_Init() == -1){
	puts("TTF didn't init properly");
	exit(-1);
    }

    TTF_Font* font = TTF_OpenFont(font_file, 14);
    gs->font = font;

    SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO);

    // SDL_WINDOW_FULLSCREEN_DESKTOP|SDL_WINDOW_ALLOW_HIGHDPI); for full screen - but figure why this
    // makes the screen stay as last view after quiting the program (screen is stuck forever!)

    gs->window = SDL_CreateWindow("Game window",
			      SDL_WINDOWPOS_UNDEFINED,
			      SDL_WINDOWPOS_UNDEFINED,
			      800,
			      600,
			      0);

    //the PRESENTVSYNC flag syncs up the renderer with the monitors refresh cycly for smooth non-tearing movement.
    gs->renderer = SDL_CreateRenderer(gs->window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
    SDL_SetRenderDrawBlendMode(gs->renderer, SDL_BLENDMODE_BLEND);

    Entity player = {.x = 220,
		     .y = 140,
		     .h = 48,
		     .w = 32,
		     .name="Aeris",
		     .type = PLAYER,
		     .subtype.na = NULL,
		     .direction = DOWN,
		     .textures = NULL,
		     .sprite = NULL,
		     .ppt = NULL,
		     .rendered = 1};
    
    Entity npc_1 = {.x = 280,
		    .y = 140,
		    .h = 48,
		    .w = 32,
		    .name="Luneth",
		    .type = NPC,
		    .subtype.na = NULL,
		    .direction = DOWN,
		    .textures = NULL,
		    .sprite = NULL,
		    .ppt = NULL,
		    .rendered = 1};

    Entity first_item = {.x = 280,
			 .y = 380,
			 .h = 32,
			 .w = 32,
			 .name = NULL,
			 .type = ITEM,
			 .subtype.itemtype = PHEONIX_FEATHER,
			 .direction = DOWN,
			 .textures = NULL,
			 .sprite = NULL,
			 .ppt = NULL,
			 .rendered = 1};

    Entity second_item = {.x = 380,
			  .y = 280,
			  .h = 32,
			  .w = 32,
			  .name = NULL,
			  .type = ITEM,
			  .subtype.itemtype = PHEONIX_FEATHER,
			  .direction = DOWN,
			  .textures = NULL,
			  .sprite = NULL,
			  .ppt = NULL,
			  .rendered = 1};

    gs->player = &player;
    gs->entities = new_list();

    list_append(gs->entities,&npc_1);
    list_append(gs->entities,&first_item);
    list_append(gs->entities,&second_item);

    load_sprites("aerithgainsborough", gs->player); 
    load_sprites("luneth", (Entity*)list_get(gs->entities,0)); 
    load_sprites("pheonixfeather", (Entity*)list_get(gs->entities,1)); 
    load_sprites("pheonixfeather", (Entity*)list_get(gs->entities,2)); 
    
    gs->player->sprite = gs->player->textures[0];

    List* e_list = gs->entities;
    for (Node* nd = e_list->head; nd != NULL; nd = nd->next){
	set_sprite((Entity*)nd->data, 0);
    }

    int playing = 1;
    while (playing){
	playing = process_events();
	render_screen();
    }

    //End of session - clean up memory
    list_destroy_include_data(gs->inventory);
    for (Node* nd = e_list->head; nd != NULL; nd = nd->next){
	Entity* e = (Entity*)nd->data;
	xfree(e->textures);
    }
    list_destroy(gs->entities);
    xfree(gs->player->textures);

    TTF_CloseFont(gs->font);
    TTF_Quit();

    //Mix_CloseAudio();
    //Mix_Quit();

    SDL_DestroyRenderer(gs->renderer);
    SDL_DestroyWindow(gs->window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
