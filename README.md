There is a makefile included in src for building.

Install valgrind to check memory safety (and other errors it can catch as a profiler). Run with

`valgrind -v --leak-check=yes --track-origins=yes ./game.exe`

and pay attention in particular to LEAK SUMMARY. There should be no lost bytes ever. 'Still reachable' is stuff that's ok to be left for the OS to clean up after the program ends.
Also check the error contexts. Some things are caused by SDL, (atm there are 3 such contexts), but if you cause any, fix them!!! 

We are using SDL2 to handle the IO. To install on linux (Debian based):

`sudo apt install libsdl2-dev`

This may or may not include the other SDL pieces necessary, but if not:

`sudo apt install libsdl2-image-dev`
`sudo apt install libsdl2-ttf-dev`
`sudo apt install libsdl2-mixer-dev`

I have no idea how to install these on Windows. Before trying to compile on Windows, change the path to the font that
TTF_OpenFont has to something that exists. Hopefully it will compile.. but probably not.
