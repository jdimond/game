#pragma once

#include"datastructs.h"
#include"linkedlist.h"

void* xmalloc(size_t sz);
void* xcalloc(size_t num, size_t sz);
List* read_dialogue_file(char* d_file);
Direction opposite_direction(Direction d);
void set_sprite(Entity* e, int index);
