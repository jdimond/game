
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define xfree(ptr) {free(ptr); ptr=NULL;}

#include"linkedlist.h"

/* Standalone linked list implementation. */

static void* xmalloc(size_t sz){
    void* tmp_ptr = malloc(sz);
    if (!tmp_ptr){
	puts("malloc failed");
	exit(1);
    }
    return tmp_ptr;
}

static void* xcalloc(size_t num, size_t sz){
    void* tmp_ptr = calloc(num,sz);
    if (!tmp_ptr){
	puts("calloc failed");
	exit(1);
    }
    return tmp_ptr;
}


List* new_list(){
    List* lst = xmalloc(sizeof(List));
    lst->head = NULL;
    lst->tail = NULL;
    return lst;
}

int list_length(List* lst){
    Node* nd = lst->head;
    int length = 0;
    while (nd){
	++length;
	nd = nd->next;
    }
    return length;
}

void list_prepend(List* lst, void* data){
    Node* nd = xmalloc(sizeof(Node));
    nd->data = data;
    nd->next = lst->head;
    nd->prev = NULL;
    if (nd->next == NULL){
	lst->head = nd;
	lst->tail = nd;
	return;
    }
    lst->head->prev = nd;
    lst->head = nd;
}
    
void list_append(List* lst, void* data){
    Node* nd = xmalloc(sizeof(Node));
    nd->data = data;
    nd->next = NULL;
    nd->prev = lst->tail;
    if (nd->prev == NULL){
	lst->head = nd;
	lst->tail = nd;
	return;
    }
    lst->tail->next = nd;
    lst->tail = nd;
}

void list_remove(List* lst, int index){
    Node* nd = lst->head;
    while (index){
	if (!nd) {
	    printf("ERROR: Linked list asked for element out of bounds\n");
	    return;
	}
	nd = nd->next;
	--index;
    }
    nd->prev->next = nd->next;
    nd->next->prev = nd->prev;
    xfree(nd);
}

void* list_get(List* lst, int index){
    Node* nd = lst->head;
    while (index){
	if (nd == lst->tail && index != 0){
	    printf("ERROR: Linked list asked for element out of bounds\n");
	    return 0;
	}
	nd = nd->next;
	--index;
    }
    return nd->data;
}

int list_set(List* lst, int index, void* data){
    Node* nd = lst->head;
    while (index){
	if (!nd){
	    return EXIT_FAILURE;
	}
	nd = nd->next;
	--index;
    }
    nd->data = data;
    return EXIT_SUCCESS;
}

    
void list_destroy(List* lst){
    Node* nd = lst->head;
    Node* next;
    while (nd){
	next = nd->next;
	xfree(nd);
	nd = next;
    }
    xfree(lst);
}

void list_destroy_include_data(List* lst){
    Node* nd = lst->head;
    Node* next;
    while (nd){
	next = nd->next;
	xfree(nd->data);
	xfree(nd);
	nd = next;
    }
    xfree(lst);
}

List* string_split(char* string, char delim){
    char* current_char = string;
    char* word_buffer;
    
    List* result = new_list();

    int word_start_index = 0;
    int word_end_index = 0;
    int word_length = 0;
    
    for (; *current_char != '\0'; ++current_char){
	if (*current_char == delim){

	    //get the word and put it into word_buffer
	    word_length = word_end_index - word_start_index;
	    word_buffer = xcalloc((word_length+1),sizeof(char)); //calloc incidentally ends buffer with null character!
	    strncpy(word_buffer, string+word_start_index, word_length); //pointer arithmetic

	    //put it in a new node
	    list_append(result, word_buffer);

	    //set indices to index next word, ignoring repeated delim chars
	    while (*(current_char + 1) == delim){ //while next char is a delim - loop will increment to non-delim
		++word_end_index;
		++current_char;
	    }
	    ++word_end_index; //because we stopped on the last delim char, this now indexes first letter of next word.
	    word_start_index = word_end_index;
	    continue;
	}
	else if (*(current_char+1) == '\0'){ //if next char is end of input string.
	    word_length = word_end_index - word_start_index + 1; //+1 to compensate not being on character after word's end.
	    word_buffer = xcalloc((word_length+1),sizeof(char));
	    strncpy(word_buffer, string+word_start_index, word_length);
	    list_append(result, word_buffer);
	    xfree(word_buffer);
	    break;
	}
	word_end_index++;
    }

    return result;
}
    
