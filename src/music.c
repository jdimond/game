#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

#include<SDL2/SDL.h>
#include<SDL2/SDL_mixer.h>

#include"datastructs.h"

extern gamestate* gs;

void play_music(){

    Mix_Init(MIX_INIT_OGG);

    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY,MIX_DEFAULT_FORMAT, 2, 1024) == -1){
	printf("Mix_OpenAudio: %s\n", Mix_GetError());
    }
    
    Mix_Music* music = Mix_LoadMUS("sounds/music/Retribution.ogg"); //some "rpg music" I found.
    if (!music){
	puts("music failed to load");
	return;
    }
    
    if (Mix_PlayMusic(music,-1) == -1){
	printf("Mix_PlayMusic: %s\n", Mix_GetError());
    }
}
