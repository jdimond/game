
#include<stdio.h>
#include<stdlib.h>

#define xfree(ptr) {free(ptr); ptr=NULL;}

#include"linkedlist.h"
#include"utils.h"
#include"datastructs.h"
#include"rendering.h"
#include"collision_detection.h"
#include"process_events.h"

static inline int next_step(int step);

extern gamestate* gs;

int process_events(){

    gs->time += 1;
    
    SDL_Event event;
    int playing = 1;

    Entity* player = gs->player;
    
    const Uint8 *state = SDL_GetKeyboardState(NULL);

    while (SDL_PollEvent(&event) && !event.key.repeat){ //ignores held keys.
	switch (event.type){
	    case SDL_WINDOWEVENT_CLOSE:
		if (gs->window){
		    SDL_DestroyWindow(gs->window);
		    gs->window = NULL;
		    playing = 0;
		}
		break;

	    case SDL_KEYDOWN:
		switch (event.key.keysym.sym){
		    case SDLK_ESCAPE:
		    case SDLK_q:
			return 0;

		    case SDLK_i:
			gs->show_inventory = 1;
			break;

		    case SDLK_x:;
			Entity* object;
			if ((object = collision_detect(player, TALK))){
			    //Make entity face player, then talk
			    assert(object->type == NPC);
			    object->direction = opposite_direction(player->direction);
			    object->sprite = object->textures[object->direction];
			    perform_dialogue();
			    break;
			}
			else if ((object = collision_detect(player, PICKUP))){
			    puts("Picking up an item");
			    assert(object->type == ITEM);

			    //make item from object's info
			    Item* item = xmalloc(sizeof(Item));
			    item->type = object->subtype.itemtype;
			    item->quantity = 1;
			    item_pickup(item);

			    //remove item from world
			    object->rendered = 0;
			}
			break;

		    case SDLK_SPACE:;
			Entity* e = (Entity*)gs->entities->head->data;
			Entity* tmp;
			tmp = player;
			gs->player = e;
			gs->entities->head->data = tmp;
			break;
			
		    case SDLK_UP:
			if (state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_RIGHT]) break;
			player->sprite = player->textures[12+3]; //first step of walking cycle.
			break;

		    case SDLK_DOWN:
			if (state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_RIGHT]) break;
			player->sprite = player->textures[0+3];
			break;

		    case SDLK_LEFT:
			if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_DOWN]) break;
			player->sprite = player->textures[4+3];
			break;

		    case SDLK_RIGHT:
			if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_DOWN]) break;
			player->sprite = player->textures[8+3];
			break;

		    default:
			break;
		}
		break;

	    case SDL_KEYUP:
		switch (event.key.keysym.sym){
		    case SDLK_UP:
			if (state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_RIGHT]) break;
			player->sprite = player->textures[UP]; //when no longer moving, set to standing sprite..
			player->direction = UP;
			break;

		    case SDLK_DOWN:
			if (state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_RIGHT]) break;
			player->sprite = player->textures[DOWN];
			player->direction = DOWN;
			break;

		    case SDLK_LEFT:
			if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_DOWN]) break;
			player->sprite = player->textures[LEFT];
			player->direction = LEFT;
			break;

		    case SDLK_RIGHT:
			if (state[SDL_SCANCODE_UP] || state[SDL_SCANCODE_DOWN]) break;
			player->sprite = player->textures[RIGHT];
			player->direction = RIGHT;
			break;

		    default:
			break;
		}
		break;

	    case SDL_QUIT:
		playing = 0;
		break;

	    default:
		break;
	}
    }

    
    //motion handling for held keys.
    int stepped = 0;
    if (state[SDL_SCANCODE_UP]){
	if (gs->time % 11 == 0)
	    gs->player->sprite = gs->player->textures[12 + next_step(gs->step)];
	gs->player->y -= 2;
	gs->step += 1;
	stepped = 1;
	if (collision_detect(gs->player, WALK)) gs->player->y +=2;
    }

    if (state[SDL_SCANCODE_DOWN]){
	if (!stepped && gs->time % 11 == 0)
	    gs->player->sprite = gs->player->textures[next_step(gs->step)];
	gs->player->y += 2;
	gs->step += 1;
	stepped = 1;
	if (collision_detect(gs->player, WALK)) gs->player->y -=2;
    }

    if (state[SDL_SCANCODE_LEFT]){
	if (!stepped && gs->time % 11 == 0)
	    gs->player->sprite = gs->player->textures[4 + next_step(gs->step)];
	gs->player->x -= 2;
	gs->step += stepped ? 0 : 1; //fixes animation in the case of diagonal movement.
	if (collision_detect(gs->player, WALK)) gs->player->x +=2;
    }

    if (state[SDL_SCANCODE_RIGHT]){
	if (!stepped && gs->time % 11 == 0)
	    gs->player->sprite = gs->player->textures[8 + next_step(gs->step)];
	gs->player->x += 2;
	gs->step += stepped ? 0 : 1;
	if (collision_detect(gs->player, WALK)) gs->player->x -=2;
    }

    
    return playing;
}


static inline int next_step(int step){
    printf("step %i, %i, %ld, location: %i,%i\n",step, step%4, gs->time, gs->player->x, gs->player->y);
    return step%4;
}


//Increase item count in inventory, or add new entry.
void item_pickup(Item* item){
    List* inv = gs->inventory;
    for (Node* nd = inv->head; nd != NULL; nd = nd->next){
	Item* entry = (Item*)nd->data;
	if (item->type == entry->type){
	    xfree(item); //not needed in list.
	    entry->quantity += 1;
	    return;
	}
    }
    //if not found, add to the inventory.
    list_append(inv, item);
}
