
#include<stdio.h>
#include<stdlib.h>

#define xfree(ptr) {free(ptr); ptr=NULL;}

#include"linkedlist.h"
#include"datastructs.h"

static inline int axis_intersection(int A, int B, int a, int b);

extern gamestate* gs;


//Returns a pointer to the entity which player will interact with. 
Entity* collision_detect(Entity* player, Interaction inter){ //how well will this do when there are lots of entities?

    int px = player->x;
    int py = player->y;
    int pw = player->w;
    int ph = player->h;

    List* entities = gs->entities;
    for (Node* nd = entities->head; nd != NULL; nd = nd->next){

	Entity* e = (Entity*)nd->data;

	if (!e->rendered) continue; //who cares about ghosts. At the moment this means picked up items will no longer be rendered.

	int ex = e->x;
	int ey = e->y;
	int ew = e->w;
	int eh = e->h;

	//In every case, looks to see if both the x interval and y interval for a pair of entities
	//overlap. If both do, there must be an overlap of the rectangles, and thus a collision must have happened.
	//The cases only differ in which regions of space are being considered for collision/overlap.
	switch (inter){
	    case WALK:
		if (axis_intersection(px, px+pw, ex+5, ex+ew-5)
		    && axis_intersection(py, py+ph, ey, ey+eh)){
		    return e;
		}
		break;
		
	    case TALK:
		if (e->type != NPC) continue;
		switch (gs->player->direction){
		    case UP:
			if (axis_intersection(px, px+pw, ex, ex+ew)
			    && axis_intersection(py-ph, py, ey, ey+eh)){
			    return e;
			}
			break;

		    case DOWN:
			if (axis_intersection(px, px+pw, ex, ex+ew)
			    && axis_intersection(py+ph, py+2*ph, ey, ey+eh)){
			    return e;
			}
			break;

		    case LEFT:
			if (axis_intersection(px-pw, px, ex, ex+ew)
			    && axis_intersection(py, py+ph, ey, ey+eh)){
			    return e;
			}
			break;

		    case RIGHT:
			if (axis_intersection(px+pw, px+2*pw, ex, ex+ew)
			    && axis_intersection(py, py+ph, ey, ey+eh)){
			    return e;
			}
			break;

		    default:
			return 0;
		}
		break;
		
	    case PICKUP:
		if (e->type != ITEM) continue;
		switch (gs->player->direction){
		    case UP:
			if (axis_intersection(px, px+pw, ex, ex+ew)
			    && axis_intersection(py-ph, py, ey, ey+eh)){
			    return e;
			}
			break;

		    case DOWN:
			if (axis_intersection(px, px+pw, ex, ex+ew)
			    && axis_intersection(py+ph, py+2*ph, ey, ey+eh)){
			    return e;
			}
			break;

		    case LEFT:
			if (axis_intersection(px-pw, px, ex, ex+ew)
			    && axis_intersection(py, py+ph, ey, ey+eh)){
			    return e;
			}
			break;

		    case RIGHT:
			if (axis_intersection(px+pw, px+2*pw, ex, ex+ew)
			    && axis_intersection(py, py+ph, ey, ey+eh)){
			    return e;
			}
			break;

		    default:
			return 0;
		}
		break;
	}
    }
    return 0;
}


static inline int axis_intersection(int A, int B, int a, int b){
    return ((b >= A) && (B >= a)) ? 1 : 0;
}
