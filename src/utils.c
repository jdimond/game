#include<stdio.h>
#include<stdlib.h>

#include"utils.h"
#include"linkedlist.h"

#define xfree(ptr) {free(ptr); ptr=NULL;}

/* A file to place things which are:
 * - Generic and commonly used
 * - Special, but should basically never be edited
 * - Convenience functions
 */

extern gamestate* gs;

void* xmalloc(size_t sz){
    void* tmp_ptr = malloc(sz);
    if (!tmp_ptr){
	puts("malloc failed");
	exit(1);
    }
    return tmp_ptr;
}


void* xcalloc(size_t num, size_t sz){
    void* tmp_ptr = calloc(num,sz);
    if (!tmp_ptr){
	puts("calloc failed");
	exit(1);
    }
    return tmp_ptr;
}

List* read_dialogue_file(char* d_file){
    FILE* f = fopen(d_file, "r");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    char* file_string = xmalloc(fsize + 1);
    fread(file_string, fsize, 1, f);
    file_string[fsize]='\0'; //null terminate the damn string!!
    fclose(f);
    List* return_value = string_split(file_string, '\n');
    xfree(file_string);
    return return_value;
}

Direction opposite_direction(Direction d){
    switch (d){
	case UP:
	    return DOWN;
	case DOWN:
	    return UP;
	case LEFT:
	    return RIGHT;
	case RIGHT:
	    return LEFT;
	default:
	    return DOWN;
    }
}

void set_sprite(Entity* e, int index){
    e->sprite = e->textures[index];
}
