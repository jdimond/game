
#pragma once

typedef struct node {
    struct node* next;
    struct node* prev;
    void* data;
} Node;

typedef struct list {
    Node* head;
    Node* tail;
} List;

List* new_list();
int list_length(List* lst);
void list_prepend(List* lst, void* data);
void list_append(List* lst, void* data);
void list_remove(List* lst, int index);
void* list_get(List* lst, int index);
int list_set(List* lst, int index, void* data);
void list_destroy(List* lst);
List* string_split(char* line, char delim);
void list_destroy_include_data(List* lst);
