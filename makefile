EXE_OUT = game.exe
CC = gcc
CFLAGS = -g -Wall -Wextra

#location of source files relative to makefile
SRC_DIR = src
#target directory for obj files relative to makefile
OBJ_DIR = bld
#target directory for dependencies relative to makefile
DEP_DIR = bld/dep

LIBS = #nothing here yet....
#add windows specific linker stuff
ifeq ($(OS),Windows_NT)
	LIBS += -lmingw32 -mwindows -lSDL2main
endif
#add remaining linker stuff
LIBS += -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer

_SRCS = main.c\
	utils.c\
	linkedlist.c\
	music.c\
	collision_detection.c\
	process_events.c\
	rendering.c\

SRCS = $(patsubst %,$(SRC_DIR)/%,$(_SRCS))
OBJS = $(patsubst %.c,$(OBJ_DIR)/%.o,$(_SRCS))
DEPS = $(patsubst %.c,$(DEP_DIR)/%.d,$(_SRCS))

$(EXE_OUT): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(LIBS) -o $@
	
$(OBJ_DIR)/%.o :
	$(CC) $(CFLAGS) $(LIBS) -c $< -o $@

clean:
	rm -f $(OBJ_DIR)/*.o $(DEP_DIR)/*.d $(EXE_OUT)

$(DEP_DIR)/%.d: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -MF"$@" -MG -MM -MP -MT"$(OBJ_DIR)/$*.o" "$<"

include $(DEPS)
