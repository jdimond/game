
#pragma once

#include<stdint.h>
#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>

#include"linkedlist.h"

typedef enum { UP=12, DOWN=0, LEFT=4, RIGHT=8 } Direction; //Numbers coincide with sprite orientation

typedef enum { PLAYER, NPC, ENEMY, ITEM } Entitytype;

typedef enum { PHEONIX_FEATHER } Itemtype;

typedef enum { ENEMY_ONE } Enemytype;

typedef enum { WALK, TALK, PICKUP } Interaction;

typedef struct Properties { //Define the extraneous stats of whatever entities might need them.
    int hp, mp, lvl;
} Properties;


typedef struct Entity {
    int x,y,w,h;            //entity's position and rect size
    Direction direction;    //entity's facing orientation
    int rendered;           //is rendered on screen?

    Entitytype type;

    union {
	void* na;           //clumsy null-value, becasue unions are not allowed to be null.
	Itemtype itemtype;
	Enemytype enemytype;
    } subtype;

    SDL_Texture** textures; //contains the possible sprites.
    SDL_Texture* sprite;    //the active sprite.

    Properties* ppt;        //entities properties, hp, mp, etc.
    char* name;             //entity name
} Entity;


typedef struct Item {       //Entries in Inventory's list.
    Itemtype type;
    int quantity;
} Item;


typedef struct Inventory {
    List* items;
} Inventory;


typedef struct gamestate {
    //Buffers, counters, flags.
    uint64_t time;
    uint64_t step;
    char* speech;
    int entity_count;    //Maybe change these to bool (stdbool) ?
    int show_inventory;
    
    //GUI
    SDL_Window* window;
    SDL_Renderer* renderer;
    TTF_Font* font;
    SDL_Rect* textrect;
    SDL_Texture* texttexture;
    int show_text;
    SDL_Rect* inventory_rect;
    
    //Player-related state.
    Entity* player;
    List* inventory;    //list of Items

    //Ambient state.
    List* entities;
} gamestate;
